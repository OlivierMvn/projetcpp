#ifndef PARTICULIER_H
#define PARTICULIER_H

#include "Client.h"

enum class SituationFamiliale {Celibataire, Marie, Divorce, Autre};

class Particulier : virtual public Client
{
    public:
        Particulier(string="\0",string="\0",char=0,long=0,string="\0",
                    long=0, SituationFamiliale=SituationFamiliale::Autre);
        virtual ~Particulier();

        long GetDateDeNaissance() { return DateDeNaissance; }
        void SetDateDeNaissance(long val) { DateDeNaissance = val; }
        string GetSitu();
        void setSitu(SituationFamiliale);
        virtual void Infos() override;

    protected:

    private:
        long DateDeNaissance;
        SituationFamiliale situFam;
};

#endif // PARTICULIER_H
