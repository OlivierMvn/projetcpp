#include "Professionnel.h"

Professionnel::Professionnel(string N,string P,char S,long T,string Ad,
                             unsigned long sir, string Rais, int an, string AdEnt, string em)
                             :Client(N,P,S,T,Ad)
{
    SetSiret(sir);
    SetRaisonSociale(Rais);
    SetAnneeCrea(an);
    SetAdresseEntreprise(AdEnt);
    SetMail(em);
}

Professionnel::~Professionnel()
{
    cout << "Destruction du Client professionnel de siret: " << GetSiret() << endl;
}

void Professionnel::Infos()
{
    Client::Infos();
    cout << "Num�ro de Siret: " << GetSiret() << endl;
    cout << "Raison Social: " << GetRaisonSociale() << endl;
    cout << "Annee de creation: " << GetAnneeCrea() << endl;
    cout << "Adresse de l'entreprise: " << GetAdresseEntreprise() << endl;
    cout << "Mail professionnel: " << GetMail() << endl;
    cout << "------------------------------------"<< endl;
}
