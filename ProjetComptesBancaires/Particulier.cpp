#include "Particulier.h"

Particulier::Particulier(string N,string P,char S,long T,string Ad,
                         long D, SituationFamiliale Sit)
                         :Client(N,P,S,T,Ad)
{
    SetDateDeNaissance(D);
    setSitu(Sit);
}

Particulier::~Particulier()
{
    cout << "Destruction du Client particulier ne le: " << GetDateDeNaissance() << endl;
}

string Particulier::GetSitu()
{
    string libelle="\0";
    switch(situFam)
    {
        case SituationFamiliale::Celibataire:
            libelle= "Celibataire";
            break;
        case SituationFamiliale::Marie:
            libelle= "Marie(e)";
            break;
        case SituationFamiliale::Divorce:
            libelle= "Divorce(e)";
            break;
        default:
            libelle= "Autre";
            break;
    }
    return libelle;
}
void Particulier::setSitu(SituationFamiliale situ)
{
    situFam = situ;
}

void Particulier::Infos()
{
    Client::Infos();
    cout << "Ne le: " << GetDateDeNaissance() << endl <<
            "Situation familiale " << GetSitu() << endl <<
            "-------------------------" << endl;
}
