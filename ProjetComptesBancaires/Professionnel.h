#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H

#include "Client.h"


class Professionnel : virtual public Client
{
    public:
        Professionnel(string="\0",string="\0",char=0,long=0,string="\0",
                      unsigned long = 0, string="\0", int=0, string="\0", string="\0");
        virtual ~Professionnel();

        unsigned long GetSiret() { return Siret; }
        void SetSiret(unsigned long val) { Siret = val; }
        string GetRaisonSociale() { return RaisonSociale; }
        void SetRaisonSociale(string val) { RaisonSociale = val; }
        int GetAnneeCrea() { return AnneeCrea; }
        void SetAnneeCrea(int val) { AnneeCrea = val; }
        string GetAdresseEntreprise() { return AdresseEntreprise; }
        void SetAdresseEntreprise(string val) { AdresseEntreprise = val; }
        string GetMail() { return Mail; }
        void SetMail(string val) { Mail = val; }

        virtual void Infos() override;

    protected:

    private:
        double Siret;
        string RaisonSociale;
        int AnneeCrea;
        string AdresseEntreprise;
        string Mail;
};

#endif // PROFESSIONNEL_H
