#include "Client.h"

Client::Client(string N,
               string P,
               char S,
               long T,
               string Ad)
        :Nom(N),Prenom(P),Sexe(S),Tel(T),AdressePostale(Ad)
{}

Client::~Client()
{
    cout << "Destruction du Client " << GetNom() << " " << GetPrenom() << endl;
}

void Client::Infos()
{
    if (GetSexe()=='H')
    {
        cout << "M ";
    }
    else
    {
        cout << "Mme ";
    }
    cout << GetNom() << " " << GetPrenom() << endl;
    cout << "Numero de telephone: " << GetTel() << endl;
    cout << "Adresse: " << GetAdressePostale() <<endl;
}
