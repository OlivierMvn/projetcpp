#ifndef COMPTEBANCAIRE_H
#define COMPTEBANCAIRE_H
#include <vector>
#include <algorithm>
#include "Professionnel.h"
#include "Particulier.h"


class CompteBancaire : public Professionnel, public Particulier
{
    public:
        CompteBancaire(string="\0",string="\0",char=0,long=0,string="\0",
                      unsigned long = 0, string="\0", int=0, string="\0", string="\0",
                      long=0, SituationFamiliale=SituationFamiliale::Autre,
                      int=0, int=0, int=0,int=0);

        virtual ~CompteBancaire();

        int GetNumCompte() { return NumCompte; }
        void SetNumCompte(int val) { NumCompte = val; }
        int GetDateOuverture() { return DateOuverture; }
        void SetDateOuverture(int val) { DateOuverture = val; }
        long GetSolde() { return Solde; }
        void SetSolde(long val) { Solde = val; }
        int GetMontantDecouvert() { return MontantDecouvert; }
        void SetMontantDecouvert(int val) { MontantDecouvert = val; }
        void operation(int, long, int, int);

        void Infos() override;
        void Ajouter(CompteBancaire*);
        void Supprimer(string);
        void Operation(int, int, int);
        int getEffectif();
        void rechByNomSolde(string);
        void tri();
        void ListerClient();



    protected:

    private:
        int NumCompte;
        int DateOuverture;
        double Solde;
        int MontantDecouvert;
        vector<CompteBancaire*> VecCompte;
        string nomARech;
};

#endif // COMPTEBANCAIRE_H
