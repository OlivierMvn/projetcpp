#include "CompteBancaire.h"

CompteBancaire::CompteBancaire(string N,string P,char S,long T,string Ad,
                             unsigned long sir, string Rais, int an, string AdEnt, string em,
                             long D, SituationFamiliale Sit,
                             int numC, int DateOuv, int Solde,int MDecouv)
                             :Client(N,P,S,T,Ad),Professionnel(N,P,S,T,Ad,sir,Rais,an,AdEnt,em),
                             Particulier(N,P,S,T,Ad,D,Sit)
{
    SetNumCompte(numC);
    SetDateOuverture(DateOuv);
    SetSolde(Solde);
    SetMontantDecouvert(MDecouv);
}

CompteBancaire::~CompteBancaire()
{
    for(int i=0; i < VecCompte.size() ; i++)
    {
        delete VecCompte[i];
    }
    cout << "Destruction des comptes bancaires" << endl;
}

void CompteBancaire::Ajouter(CompteBancaire* C)
{
    VecCompte.push_back(C);
}

void CompteBancaire::Supprimer(string nomsup)
{
    for(auto v: VecCompte)
    {
        if (v->GetNom()==nomsup)
        {
            delete v;
            VecCompte.shrink_to_fit();
        }
    }
    cout << endl;
}

void CompteBancaire::Operation(int numducompte, int op, int mont)
{

    for(int i=0; i < VecCompte.size(); i++)
    {
        if (VecCompte.at(i)->GetNumCompte()==numducompte)
        {

            if(op==1 || op==2)
            {
                if(VecCompte.at(i)->GetSolde()-mont>VecCompte.at(i)->GetMontantDecouvert())
                {
                    VecCompte.at(i)->SetSolde(VecCompte.at(i)->GetSolde()-mont);
                }
                else
                {
                    cout << "Montant trop eleve pour le decouvert !!!" << endl;
                }
            }
            if(op==3)
            {
                VecCompte.at(i)->SetSolde(VecCompte.at(i)->GetSolde()+mont);
            }
        }
    }
}

void CompteBancaire::Infos()
{
    for(auto elm : VecCompte)
    {
    if(elm->GetSiret()==0)
    {
        cout << "Particulier: " << elm->GetNumCompte() << endl;

    if (elm->GetSexe()=='H')
    {
        cout << "M ";
    }
    else
    {
        cout << "Mme ";
    }
    cout << elm->GetNom() << " " << elm->GetPrenom() << endl;
    cout << "Adresse: " << elm->GetAdressePostale() <<endl << endl;
    cout << "Numero de telephone: " << elm->GetTel() << endl;
    cout << "Situation Familiale: " << elm->GetSitu() << endl;
    cout << "Ne le: " << elm->GetDateDeNaissance() << endl;

    cout << endl << "---------------------------------------" << endl;
    }
    else
    {
    cout << "Professionnel: " << elm->GetNumCompte() << endl;
    cout << "Siret: " << elm->GetSiret() << endl;
    cout << "Raison Social: " << elm->GetRaisonSociale() << endl;
    cout << "Annee de creation: " << elm->GetAnneeCrea() << endl;
    cout << "Adresse de l'entreprise: " << elm->GetAdresseEntreprise() << endl<< endl;
    cout << elm->GetNom() << " " << elm->GetPrenom() << endl;
    cout << "Numero de telephone: " << elm->GetTel() << endl;
    cout << "Mail professionnel: " << elm->GetMail() << endl;
    cout << endl << "---------------------------------------" << endl;
    }

    }
}

int CompteBancaire::getEffectif()
{
    return count_if(VecCompte.begin(), VecCompte.end(), [](auto p){return true;});
}
void CompteBancaire::tri()
{
    sort(VecCompte.begin(), VecCompte.end());
}

void CompteBancaire::rechByNomSolde(string nomARech)
{
    for(int i=0; i < VecCompte.size(); i++)
    {
        if (VecCompte.at(i)->GetNom()==nomARech)
        {
            cout << "Compte: " << VecCompte.at(i)->GetNumCompte() << " a " << VecCompte.at(i)->GetSolde() << "euros" << endl;
        }
    }
    cout << endl;
}

void CompteBancaire::ListerClient()
{
    if (this->getEffectif() > 0)
    {
        for(auto v : VecCompte)
        {
            cout << v->GetNom() << " " << v->GetPrenom() << endl;
        }
    }
    else
    {
        cout << "---------Pas de Clients-----------" << endl;
    }
}


