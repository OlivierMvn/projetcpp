#ifndef CLIENT_H
#define CLIENT_H
#include<algorithm>
#include <cctype>
#include<iostream>

using namespace std;

struct my_toupper
{
    char operator()(char c) const
    {
        return std::toupper(static_cast<unsigned char>(c));
    }
};

class Client
{
    public:
        Client(string="\0",string="\0",char=0,long=0,string="\0");
        virtual ~Client();

        string GetNom() {
            transform(Nom.begin(), Nom.end(), Nom.begin(), my_toupper());
            return Nom; }
        void SetNom(string val) { Nom = val; }
        string GetPrenom() { return Prenom; }
        void SetPrenom(string val) { Prenom = val; }
        char GetSexe() { return Sexe; }
        void SetSexe(char val) { Sexe = val; }
        long GetTel() { return Tel; }
        void SetTel(long val) { Tel = val; }
        string GetAdressePostale() { return AdressePostale; }
        void SetAdressePostale(string val) { AdressePostale = val; }

        virtual void Infos()=0;

    protected:

    private:
        string Nom;
        string Prenom;
        char Sexe;
        long Tel;
        string AdressePostale;
};

#endif // CLIENT_H
